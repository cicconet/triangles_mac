//
//  main.cpp
//  Triangles
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#include <iostream>
#include "PATImage.h"
#include "Triangles.h"
#include "Triangle.h"
#include "LocMax.h"
#include "LinAlg.h"
#include "EFT.h"

Point2D location_cir(Triangle t, int accWidth, int accHeight)
{
    Point2D tau_perp_p = perp(t.tauP);
    float numerator = t.m.y-t.p.y-(t.m.x-t.p.x)*tau_perp_p.y/tau_perp_p.x;
    float denominator = t.T_perp_pq.x*tau_perp_p.y/tau_perp_p.x-t.T_perp_pq.y;
    float beta = numerator/denominator;
    Point2D center = vsum(t.m, svproduct(beta, t.T_perp_pq));
    return center;
}

Point2D location_sym(Triangle t, int accWidth, int accHeight)
{
    Point2D d = t.T_pq;
    if (d.y < 0) { d = vnegative(d); }
    float gamma = atan2f(d.y, d.x);
    float displacement = dotproduct(t.m, d);
    float x = gamma/M_PI*accHeight;
    float y = (float)accWidth/2.0+displacement;
    return Point2DMake(x, y);
}

Point2D location_ske(Triangle t, int accWidth, int accHeight)
{
    return t.m;
}

Point2D location_medial_axis(Triangle t, int accWidth, int accHeight)
{
    return location_cir(t, accWidth, accHeight);
}

Point2D location_jun(Triangle t, int accWidth, int accHeight)
{
    return t.x;
}

Point2D location_par(Triangle t, int accWidth, int accHeight)
{
    return t.m;
}

float weight_cir(Triangle t, int imgWidth, int imgHeight)
{
    Point2D center = location_cir(t, imgWidth, imgHeight);
    float radius = norm(vsum(center, vnegative(t.p)));
    float w = 0.0;
    if (radius >= 45.0 && radius <= 55.0) {
        w = t.wmp;
    }
    return w;
}

float weight_sym(Triangle t, int imgWidth, int imgHeight)
{
    float w = 0.0;
    if (t.wpp < 0.9) {
        w = t.wmp;
    }
    return w;
}

float weight_ske(Triangle t, int imgWidth, int imgHeight)
{
    float w = 0.0;
    if (t.base >= 5.0 && t.base <= 20.0) {
        if (dotproduct(t.T_perp_pq, t.tauP) < -0.25 && dotproduct(t.T_perp_pq, t.tauQ) > 0.25) {
            w = t.wpp;
        }
    }
    return w;
}

float weight_medial_axis(Triangle t, int imgWidth, int imgHeight)
{
    Point2D center = location_cir(t, imgWidth, imgHeight);
    float radius = norm(vsum(center, vnegative(t.p)));
    float w = 0.0;
    if (radius >= 1.0 && radius <= 20.0 && t.wmp > 0.95) {
        w = t.wpp;
    }
    return w;
}

float weight_jun(Triangle t, int imgWidth, int imgHeight)
{
    float w = 0.0;
    if (t.base > 4.0 && t.wmp > 0.9 && t.wpp < 0.5) {
        w = t.wav_weight;
    }
    return w;
}

float weight_par(Triangle t, int imgWidth, int imgHeight)
{
    float w = 0.0;
    
    Point2D pq = vsum(t.q, vnegative(t.p));
    Point2D tp_perp = perp(t.tauP);
    float wdt = fabsf(dotproduct(pq, tp_perp));
    if (t.pp < 0.1 && dotproduct(t.T_perp_pq, t.tauP) < 0.0 && wdt > 4.0) {
        w =  t.wav_weight;
    }
    return w;
}

void raster(const char * problem);
void find_circles(void);
void find_sym_axis(void);
void q_find_sym_axis(void);
void find_skeleton(void);
void find_medial_axis(void);
void find_ellipses(void);

void paint_sym_axis(Point2D dirHistLocation, PATImage image, int accWidth, int accHeight);


// ****************************************************************************************************
int main(int argc, const char * argv[])
{
//    raster("junctions"); // "junctions" or "swt" (stroke width transform)
//    find_circles();
//    find_sym_axis();
//    q_find_sym_axis();
//    find_skeleton();
    find_ellipses();
    
    return 0;
}
// ****************************************************************************************************


void raster(const char * problem)
{
    PATImage image;
    if (strcmp(problem, "swt") == 0) {
        image.set_up_with_path("/Users/Cicconet/MacDev/Triangles_Mac/SWT.png");
    } else if (strcmp(problem, "junctions") == 0) {
        image.set_up_with_path("/Users/Cicconet/MacDev/Triangles_Mac/J3.png");
    }
    image.save_png_to_path("/Users/Cicconet/Desktop/Image.png");
    
    float scale = 1.0;
//    int kHalfWidth = 2;
    int nOrient = 32;
    int hopSize = 1;
    int halfWS = 0;
    float magThreshold = 0.1;
    bool dataStructureList = false;
    bool thresholdingLocal = true;
    bool ignoreDirection = true;
    bool accAdd = true;
    if (strcmp(problem, "swt") == 0) {
        ignoreDirection = false;
        accAdd = false;
    }
    PATCoefficients cf;
    cf.set_up(image.width, image.height, scale, nOrient, hopSize, halfWS, magThreshold, dataStructureList, thresholdingLocal, ignoreDirection);
//    cf.set_up_l(image.width, image.height, kHalfWidth, nOrient, hopSize, halfWS, magThreshold, dataStructureList, thresholdingLocal, ignoreDirection);
    cf.set_input(image);
    cf.perform_convolutions();
    cf.find_coefficients();
    cf.save_png_to_path("/Users/Cicconet/Desktop/WavOutputs.png");
    
    int khs = 3; // kernel half size
    int factor = 10;
    PATImage window; // wavelet outputs
    window.set_up_with_data(NULL,factor*(2*khs+1),factor*(2*khs+1));
    PATImage W; // image and accumulator
    W.set_up_with_data(NULL, 4*khs, 2*khs);
    PATImage paramDomain;
    paramDomain.set_up_with_data(NULL, 2*khs, 2*khs);
    PATImage A;
    A.set_up_with_data(NULL, image.width, image.height);
//    char path[100];
    float *array_m = (float *)malloc(4*khs*khs*sizeof(float));
    float *array_a = (float *)malloc(4*khs*khs*sizeof(float));
    float *array_x = (float *)malloc(4*khs*khs*sizeof(float));
    float *array_y = (float *)malloc(4*khs*khs*sizeof(float));
    for (int i = 2*khs; i < cf.nSelectedRows-2*khs; i++) {
        for ( int j = 2*khs; j < cf.nSelectedCols-2*khs; j++) {
            printf("%d/%d, %d/%d\n", i, cf.nSelectedRows, j, cf.nSelectedCols);
            window.set_zero();
            paramDomain.set_zero();
            for (int ii = -khs; ii < khs; ii++) {
                for (int jj = -khs; jj < khs; jj++) {
                    W.data[(khs+ii)*W.width+(khs+jj)] = image.data[(i+ii)*image.width+(j+jj)];
                    
                    int index = (i+ii)*cf.nSelectedCols+(j+jj);
                    float m = cf.M.data[index];
                    float a = cf.A.data[index];
                    float x = khs+ii;//cf.X.data[index];
                    float y = khs+jj;//cf.Y.data[index];
                    
                    index = (khs+ii)*2*khs+(khs+jj);
                    array_m[index] = m;
                    array_a[index] = a;
                    array_x[index] = x;
                    array_y[index] = y;
                    
                    int row0 = factor*(khs+ii+1);
                    int col0 = factor*(khs+jj+1);
                    for (int k = -factor/3; k < factor/3; k++) {
                        int row = row0+roundf(k*cosf(a));
                        int col = col0+roundf(k*sinf(a));
                        window.data[row*window.width+col] = m;
                    }
                }
            }
            
            Triangles ts;
            ts.set_up_quick(4*khs*khs);
            ts.list_triangles_quick(array_m, array_a, array_x, array_y);

            if (strcmp(problem, "swt") == 0) {
                ts.acc2(paramDomain, location_par, weight_par);
            } else if (strcmp(problem, "junctions") == 0) {
                ts.acc2(paramDomain, location_jun, weight_jun);
            }
            
            ts.clean_up_quick();
            
            for (int ii = -khs; ii < khs; ii++) {
                for (int jj = -khs; jj < khs; jj++) {
                    W.data[(khs+ii)*W.width+(2*khs+khs+jj)] = paramDomain.data[(khs+ii)*paramDomain.width+(khs+jj)];
                    float value = paramDomain.data[(khs+ii)*paramDomain.width+(khs+jj)];
                    if (accAdd) {
                        A.data[i*A.width+j] += value;
                    } else {
                        if (value > A.data[i*A.width+j]) {
                            A.data[i*A.width+j] = value;
                        }
                    }
                }
            }
            
//            sprintf(path, "/Users/Cicconet/Downloads/Images/WindowsI/I_%03d_%03d.png",i,j);
//            window.save_png_to_path(path);
//            
//            sprintf(path, "/Users/Cicconet/Downloads/Images/WindowsW/W_%03d_%03d.png",i,j);
//            W.save_png_to_path(path);
        }
    }
    
    A.normalize();
    A.save_png_to_path("/Users/Cicconet/Desktop/Acc.png");
    A.clean_up();
    
    W.clean_up();
    window.clean_up();
    paramDomain.clean_up();
    
    free(array_m);
    free(array_a);
    free(array_x);
    free(array_y);
}

void find_circles(void)
{
    PATImage image;
    image.set_up_with_path("/Users/Cicconet/MacDev/Triangles_Mac/C3.png");
    
    Triangles ts;
    float scale = 2.0;
    int nOrient = 16;
    int hopSize = 5;
    int halfWS = 1;
    float magThreshold = 0.01;
    bool ignoreDirection = true;
    ts.set_up(image.width, image.height, scale, nOrient, hopSize, halfWS, magThreshold, ignoreDirection);
    ts.set_input(image);
    ts.perform_convolutions();
    ts.find_coefficients();
    ts.save_png_to_path("/Users/Cicconet/Desktop/WavOutputs.png");
    ts.list_triangles();
    
    PATImage paramDomain;
    paramDomain.set_up_with_data(NULL, image.width, image.height);

    ts.acc2(paramDomain, location_cir, weight_cir);
    ts.clean_up();
    
    paramDomain.normalize();
    paramDomain.save_png_to_path("/Users/Cicconet/Desktop/CentHists.png");
    
    LocMax lm;
    int kernelHalfSize = 5;
    int nLocMax = 3; // maximum number local maxima to find
    float threshold = 0.5;
    lm.set_up(paramDomain, kernelHalfSize, nLocMax, threshold);
    lm.im.save_png_to_path("/Users/Cicconet/Desktop/Blur.png");
    lm.find();
    
    float radius = 50.0;
    PATImage circles;
    circles.set_up_with_data(image.data, paramDomain.width, paramDomain.height);
    for (int i = 0; i < circles.width*circles.height; i++) { circles.data[i] *= 0.25; }
    for (int i = 0; i < lm.nLocs; i++) { // nLocs = number of local maxima actually found
        int row = lm.locs[i].x;
        int col = lm.locs[i].y;
        circles.data[row*circles.width+col] = 1.0;
        int nPoints = 50;
        for (int j = 0; j < nPoints; j++) {
            float angle = (float)j/(float)nPoints*2.0*M_PI;
            int r = roundf(row+radius*cosf(angle));
            int c = roundf(col+radius*sinf(angle));
            if (r > -1 && r < circles.height && c > -1 && c < circles.width) {
                circles.data[r*circles.width+c] = 1.0;
            }
        }
    }
    circles.save_png_to_path("/Users/Cicconet/Desktop/Circles.png");
    circles.clean_up();
    
    paramDomain.clean_up();
    lm.clean_up();
    image.clean_up();
}

void find_sym_axis(void)
{
    PATImage image;
    image.set_up_with_path("/Users/Cicconet/MacDev/Triangles_Mac/S2.png");
    
    Triangles ts;
    float scale = 1.0;
    int nOrient = 32;
    int hopSize = 5;
    int halfWS = 1;
    float magThreshold = 0.02;
    bool ignoreDirection = true;
    ts.set_up(image.width, image.height, scale, nOrient, hopSize, halfWS, magThreshold, ignoreDirection);
    ts.set_input(image);
    ts.perform_convolutions();
    ts.find_coefficients();
    ts.save_png_to_path("/Users/Cicconet/Desktop/WavOutputs.png");
    ts.list_triangles();
    
    int nRows = 360;
    int nCols = 2*ceilf(sqrtf(image.height*image.height+image.width*image.width))+1;
    PATImage paramDomain;
    paramDomain.set_up_with_data(NULL, nCols, nRows); // angle (0 to 360) vs. displacement (-diagonal to diagonal)
    
    ts.acc2(paramDomain, location_sym, weight_sym);
    
    paramDomain.normalize();
    paramDomain.save_png_to_path("/Users/Cicconet/Desktop/DirHist.png");
    
    LocMax lm;
    int kernelHalfSize = 10;
    int nLocMax = 1; // maximum number local maxima to find
    float threshold = 0.5;
    lm.set_up(paramDomain, kernelHalfSize, nLocMax, threshold);
    lm.im.save_png_to_path("/Users/Cicconet/Desktop/Conv.png");
    lm.find();
    PATImage locsImage;
    locsImage.set_up_with_data(NULL, paramDomain.width, paramDomain.height);
    for (int i = 0; i < lm.nLocs; i++) {
        int row = lm.locs[i].x;
        int col = lm.locs[i].y;
        locsImage.data[row*locsImage.width+col] = 1;
    }
    locsImage.save_png_to_path("/Users/Cicconet/Desktop/LocsImage.png");
    locsImage.clean_up();
    
    for (int i = 0; i < image.width*image.height; i++) { image.data[i] *= 0.5; }
    paint_sym_axis(lm.locs[0], image, paramDomain.width, paramDomain.height);
    image.save_png_to_path("/Users/Cicconet/Desktop/SymAxis.png");
    ts.clean_up();
    
    paramDomain.clean_up();
    lm.clean_up();
    image.clean_up();
}

void q_find_sym_axis(void)
{
    PATImage image;
    image.set_up_with_path("/Users/Cicconet/MacDev/Triangles_Mac/S2.png");
    
    Triangles ts;
    float scale = 1.0;
    int nOrient = 32;
    int hopSize = 5;
    int halfWS = 1;
    float magThreshold = 0.02;
    bool ignoreDirection = true;
    ts.set_up(image.width, image.height, scale, nOrient, hopSize, halfWS, magThreshold, ignoreDirection);
    ts.set_input(image);
    ts.perform_convolutions();
    ts.find_coefficients();
    ts.save_png_to_path("/Users/Cicconet/Desktop/WavOutputs.png");
    ts.list_triangles();
    
    int nRows = 360;
    int nCols = 2*ceilf(sqrtf(image.height*image.height+image.width*image.width))+1;
    PATImage paramDomain;
    paramDomain.set_up_with_data(NULL, nCols, nRows); // angle (0 to 360) vs. displacement (-diagonal to diagonal)
    
    ts.acc2(paramDomain, location_sym, weight_sym);
    
    paramDomain.normalize();
    paramDomain.save_png_to_path("/Users/Cicconet/Desktop/DirHist.png");
    
    LocMax lm;
    int kernelHalfSize = 10;
    int nLocMax = 1; // maximum number local maxima to find
    float threshold = 0.5;
    float stDev1 = (float)kernelHalfSize/2.0;
    float stDev2 = (float)kernelHalfSize/2.0;
    float freq1 = 0.1;
    float freq2 = 0.5;
//    WaveKernelType wkt = WaveKernelTypeNoInteraction;
//    lm.set_up_q2(paramDomain, kernelHalfSize, stDev1, freq1, stDev2, freq2, wkt, nLocMax, threshold);
    lm.set_up_q2_complex(paramDomain, kernelHalfSize, stDev1, freq1, stDev2, freq2, nLocMax, threshold);
    
    lm.im.save_png_to_path("/Users/Cicconet/Desktop/Conv.png");
    lm.find();
    PATImage locsImage;
    locsImage.set_up_with_data(NULL, paramDomain.width, paramDomain.height);
    for (int i = 0; i < lm.nLocs; i++) {
        int row = lm.locs[i].x;
        int col = lm.locs[i].y;
        locsImage.data[row*locsImage.width+col] = 1;
    }
    locsImage.save_png_to_path("/Users/Cicconet/Desktop/LocsImage.png");
    locsImage.clean_up();
    
    for (int i = 0; i < image.width*image.height; i++) { image.data[i] *= 0.5; }
    paint_sym_axis(lm.locs[0], image, paramDomain.width, paramDomain.height);
    image.save_png_to_path("/Users/Cicconet/Desktop/SymAxis.png");
    ts.clean_up();
    
    paramDomain.clean_up();
    lm.clean_up();
    image.clean_up();
}

void paint_sym_axis(Point2D dirHistLocation, PATImage image, int accWidth, int accHeight)
{
    float gamma = (dirHistLocation.x-1.0)/(float)accHeight*M_PI;
    float displacement = dirHistLocation.y-(accWidth-1.0)/2.0;
    if (gamma <= M_PI/2.0) {
        Point2D v = Point2DMake(cosf(gamma), sinf(gamma));
        Point2D p = svproduct(displacement, v);
        int d = 0;
        Point2D vp = Point2DMake(-v.y, v.x);
        Point2D q = vsum(p, svproduct(d, vp));
        int qx = roundf(q.x);
        int qy = roundf(q.y);
        while (qx > -1 && qy < image.width) {
            if (qx < image.height && qy > -1) {
                image.data[qx*image.width+qy] = 1.0;
            }
            d += 1;
            q = vsum(p, svproduct(d, vp));
            qx = roundf(q.x);
            qy = roundf(q.y);
        }
        d = 1;
        q = vsum(p, svproduct(-d, vp));
        qx = roundf(q.x);
        qy = roundf(q.y);
        while (qx < image.height && qy > -1) {
            if (qx > -1 && qy < image.width) {
                image.data[qx*image.width+qy] = 1.0;
            }
            d += 1;
            q = vsum(p, svproduct(-d, vp));
            qx = roundf(q.x);
            qy = roundf(q.y);
        }
    } else {
        Point2D v = Point2DMake(cosf(gamma), sinf(gamma));
        Point2D p = svproduct(displacement, v);
        int d = 0;
        Point2D vp = Point2DMake(v.y, -v.x);
        Point2D q = vsum(p, svproduct(d, vp));
        int qx = roundf(q.x);
        int qy = roundf(q.y);
        while (qx < image.height && qy < image.width) {
            if (qx > -1 && qy > -1) {
                image.data[qx*image.width+qy] = 1.0;
            }
            d += 1;
            q = vsum(p, svproduct(d, vp));
            qx = roundf(q.x);
            qy = roundf(q.y);
        }
    }
}

void find_skeleton(void)
{
    PATImage image;
    image.set_up_with_path("/Users/Cicconet/MacDev/Triangles_Mac/CElegans.png");
    
    Triangles ts;
    float scale = 1.0;
    int nOrient = 32;
    int hopSize = 5;
    int halfWS = 1;
    float magThreshold = 0.01;
    bool ignoreDirection = false;
    ts.set_up(image.width, image.height, scale, nOrient, hopSize, halfWS, magThreshold, ignoreDirection);
    ts.set_input(image);
    ts.perform_convolutions();
    ts.find_coefficients();
    ts.save_png_to_path("/Users/Cicconet/Desktop/WavOutputs.png");
    ts.list_triangles();
    
    PATImage paramDomain;
    paramDomain.set_up_with_data(NULL, image.width, image.height);
    
    ts.acc2(paramDomain, location_ske, weight_ske);
    paramDomain.normalize();
    paramDomain.save_png_to_path("/Users/Cicconet/Desktop/MidHist.png");
    
    paramDomain.clean_up();
    ts.clean_up();
    image.clean_up();
}

void find_ellipses(void)
{
    PATImage image;
    image.set_up_with_path("/Users/Cicconet/MacDev/Triangles_Mac/E2.png");
    
    EFT eft;
    float scale = 2.0;
    int nOrient = 16;
    int hopSize = 5;
    int halfWS = 1;
    float magThreshold = 0.01;
    bool ignoreDirection = true;
    eft.set_up(image.width, image.height, scale, nOrient, hopSize, halfWS, magThreshold, ignoreDirection);
    eft.set_input(image); 
    eft.perform_convolutions();
    eft.find_coefficients();
    eft.save_png_to_path("/Users/Cicconet/Desktop/WavOutputs.png");
    eft.list_triangles();
    float minRad = 20.0;
    float maxRad = 60.0;
    PATImage centHist;
    centHist.set_up_with_data(NULL, image.width, image.height);
    eft.compute_centers_histogram(centHist, minRad, maxRad);
    centHist.normalize();
    centHist.save_png_to_path("/Users/Cicconet/Desktop/CentHists.png");
    
    LocMax lm;
    int kernelHalfSize = 5;
    int nLocMax = 3; // maximum number local maxima to find
    float threshold = 0.5;
    lm.set_up(centHist, kernelHalfSize, nLocMax, threshold);
    lm.im.save_png_to_path("/Users/Cicconet/Desktop/Blur.png");
    lm.find();
    PATImage locsImage;
    locsImage.set_up_with_data(NULL, image.width, image.height);
    for (int i = 0; i < lm.nLocs; i++) {
        int row = lm.locs[i].x;
        int col = lm.locs[i].y;
        locsImage.data[row*locsImage.width+col] = 1;
    }
    locsImage.save_png_to_path("/Users/Cicconet/Desktop/LocsImage.png");
    
    float proximityThreshold = 1.0;
    eft.cluster_by_ellipse(lm.locs, lm.nLocs, proximityThreshold);
    
    locsImage.set_zero();
    eft.paint_points_in_ellipse(locsImage, 0);
    locsImage.save_png_to_path("/Users/Cicconet/Desktop/LocsImage0.png");
//    locsImage.set_zero();
//    eft.paint_points_in_ellipse(locsImage, 1);
//    locsImage.save_png_to_path("/Users/Cicconet/Desktop/LocsImage1.png");
//    locsImage.set_zero();
//    eft.paint_points_in_ellipse(locsImage, 2);
//    locsImage.save_png_to_path("/Users/Cicconet/Desktop/LocsImage2.png");
    locsImage.clean_up();
    
    PATImage ellipses;
    ellipses.set_up_with_data(image.data, centHist.width, centHist.height);
    for (int i = 0; i < ellipses.width*ellipses.height; i++) { ellipses.data[i] *= 0.25; }
    for (int index = 0; index < lm.nLocs; index++) {
        RemEllPar rep = eft.remaining_parameters_in_ellipse(index, lm.locs[index]);
        int nPoints = 50;
        for (int i = 0; i < nPoints; i++) {
            float a = (float)i/(float)nPoints*2.0*M_PI;
            int r = roundf(lm.locs[index].x+rep.semiMajorAxis*cosf(rep.angle)*cosf(a)-rep.semiMinorAxis*sinf(rep.angle)*sinf(a));
            int c = roundf(lm.locs[index].y+rep.semiMajorAxis*sinf(rep.angle)*cosf(a)+rep.semiMinorAxis*cosf(rep.angle)*sinf(a));
            if (r > -1 && r < ellipses.height && c > -1 && c < ellipses.width) {
                ellipses.data[r*ellipses.width+c] = 1.0;
            }
        }
    }
    ellipses.save_png_to_path("/Users/Cicconet/Desktop/Ellipses.png");
    ellipses.clean_up();
    
    centHist.clean_up();
    lm.clean_up();
    eft.clean_up();
    image.clean_up();
}

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
//  Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
//  IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.
