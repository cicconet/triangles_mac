//
//  EFT.cpp
//  Triangles
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#include "EFT.h"

float arraymax(float * array, int size);
float arraymin(float * array, int size);

void EFT::set_up(int width,
                 int height,
                 float scale,
                 int nOrient,
                 int hopSize,
                 int halfWS,
                 float magThreshold,
                 bool ignoreDirection)
{
    semiMinorAxisBuffer = NULL;
    ellipseIndices = NULL;
    tPairsInd1 = NULL;
    tPairsInd2 = NULL;
    Triangles::set_up(width, height, scale, nOrient, hopSize, halfWS, magThreshold, ignoreDirection);
}

void EFT::clean_up()
{
    if (tPairsInd1) {
        free(tPairsInd1);
        free(tPairsInd2);
    }
    if (ellipseIndices) { free(ellipseIndices); }
    if (semiMinorAxisBuffer) { free(semiMinorAxisBuffer); }
    Triangles::clean_up();
}

void EFT::compute_centers_histogram(PATImage centHist, float minRadius, float maxRadius)
{
    semiMinorAxisBufferLength = (centHist.width < centHist.height ? centHist.width : centHist.height);
    semiMinorAxisBuffer = (float *)malloc(semiMinorAxisBufferLength*sizeof(float));
    int nQuadsFactor = 4;
    nQuads = nQuadsFactor*nPairs;
    tPairsInd1 = (int *)malloc(nQuads*sizeof(int));
    tPairsInd2 = (int *)malloc(nQuads*sizeof(int));
    int quadsIndex = 0;
    srand((unsigned)time(NULL));
    while (quadsIndex < nQuads) {
        int randIndex1 = rand() % nPairs; // rand number in {0,...,nPairs-1}
        int randIndex2 = rand() % nPairs;
        if (randIndex1 != randIndex2 &&
            triangles[randIndex1].wpp < 0.99 &&
            triangles[randIndex2].wpp < 0.99) {
            tPairsInd1[quadsIndex] = randIndex1;
            tPairsInd2[quadsIndex] = randIndex2;
            LineIntersection li = lineintersection(triangles[randIndex1].m,
                                                   triangles[randIndex1].d,
                                                   triangles[randIndex2].m,
                                                   triangles[randIndex2].d);
            float * array = (float *)malloc(4*sizeof(float));
            array[0] = norm(vsum(li.point, vnegative(triangles[randIndex1].p)));
            array[1] = norm(vsum(li.point, vnegative(triangles[randIndex1].q)));
            array[2] = norm(vsum(li.point, vnegative(triangles[randIndex2].p)));
            array[3] = norm(vsum(li.point, vnegative(triangles[randIndex2].q)));
            float mx = arraymax(array, 4);
            float mn = arraymin(array, 4);
            free(array);
            if (mn >= minRadius && mx <= maxRadius) {
                int row = roundf(li.point.x);
                int col = roundf(li.point.y);
                if (row > -1 && row < centHist.height && col > -1 && col < centHist.width) {
                    centHist.data[row*centHist.width+col] += 1.0;
                }
            }
            quadsIndex += 1;
        }
    }
}

void EFT::paint_points_in_ellipse(PATImage image, int ellipseIndex)
{
    for (int i = 0; i < nQuads; i++) {
        if (ellipseIndices[i] == ellipseIndex) {
            // boundary
            Point2D points[4];
            points[0] = triangles[tPairsInd1[i]].p; // p (1)
            points[1] = triangles[tPairsInd1[i]].q; // q (1)
            points[2] = triangles[tPairsInd2[i]].p; // p (2)
            points[3] = triangles[tPairsInd2[i]].q; // q (2)
            for (int j = 0; j < 4; j++) {
                image.data[(int)points[j].x*image.width+(int)points[j].y] = 1;
            }
            // center
            LineIntersection li = lineintersection(triangles[tPairsInd1[i]].m,
                                                   triangles[tPairsInd1[i]].d,
                                                   triangles[tPairsInd2[i]].m,
                                                   triangles[tPairsInd2[i]].d);
            int row = roundf(li.point.x);
            int col = roundf(li.point.y);
            image.data[row*image.width+col] = 0.5;
        }
    }
}

RemEllPar EFT::remaining_parameters_in_ellipse(int ellipseIndex, Point2D center)
{
    int nTriangles = 0;
    for (int i = 0; i < nQuads; i++) {
        if (ellipseIndices[i] == ellipseIndex) nTriangles += 2;
    }
    
    PATImage P;
    P.set_up_with_data(NULL, 3, nTriangles);
    
    nTriangles = 0;
    for (int i = 0; i < nQuads; i++) {
        if (ellipseIndices[i] == ellipseIndex) {
            Triangle t = triangles[tPairsInd1[i]];
            Point2D v = vsum(t.q, vnegative(t.p));
            Point2D w = vsum(t.x, vnegative(t.m));
            P.data[nTriangles*3] = v.x*w.x;
            P.data[nTriangles*3+1] = v.x*w.y+v.y*w.x;
            P.data[nTriangles*3+2] = v.y*w.y;
            t = triangles[tPairsInd2[i]];
            v = vsum(t.q, vnegative(t.p));
            w = vsum(t.x, vnegative(t.m));
            P.data[(nTriangles+1)*3] = v.x*w.x;
            P.data[(nTriangles+1)*3+1] = v.x*w.y+v.y*w.x;
            P.data[(nTriangles+1)*3+2] = v.y*w.y;
            nTriangles += 2;
        }
    }
    
    float PtPData[9];
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j <= i; j++) {
            float s = 0.0;
            for (int k = 0; k < nTriangles; k++) {
                s += P.data[k*3+i]*P.data[k*3+j];
            }
            PtPData[i*3+j] = s;
        }
    }
    for (int i = 0; i < 3; i++) {
        for (int j = i+1; j < 3; j++) {
            PtPData[i*3+j] = PtPData[j*3+i];
        }
    }
    P.clean_up();
    
    float eigenVectors[9];
    float eigenValues[3];
    eigdec(eigenValues, eigenVectors, PtPData, 3);
    
    float alpha = eigenVectors[0];
    float beta = eigenVectors[3];
    float gamma = eigenVectors[6];
    float B[4];
    B[0] = alpha;
    B[1] = beta;
    B[2] = beta;
    B[3] = gamma;
    
    float evc[4];
    float evl[2];
    eigdec(evl, evc, B, 2);
    
    if (evl[0] < 0) {
        for (int i = 0; i < 4; i++) {
            B[i] = -B[i];
        }
    }
    
    eigdec(evl, evc, B, 2);
    
    RemEllPar rep;
    rep.semiMinorAxis = 0.0;
    rep.semiMajorAxis = 0.0;
    rep.angle = 0.0;
    for (int i = 0; i < semiMinorAxisBufferLength; i++) {
        semiMinorAxisBuffer[i] = 0.0;
    }
    if (evl[0]*evl[1] > 0.0) {
        float sds = 0.0; // sum of distances
        Matrix2D M = matrix2d(sqrtf(evl[0])*evc[0], sqrtf(evl[0])*evc[1], sqrtf(evl[1])*evc[2], sqrtf(evl[1])*evc[3]);
        Triangle t;
        for (int i = 0; i < nQuads; i++) {
            if (ellipseIndices[i] == ellipseIndex) {
                float values[4];
                t = triangles[tPairsInd1[i]];
                values[0] = norm(mvproduct(M, vsum(t.p, vnegative(center))));
                values[1] = norm(mvproduct(M, vsum(t.q, vnegative(center))));
                t = triangles[tPairsInd2[i]];
                values[2] = norm(mvproduct(M, vsum(t.p, vnegative(center))));
                values[3] = norm(mvproduct(M, vsum(t.q, vnegative(center))));
                for (int j = 0; j < 4; j++) {
                    sds = sds+values[j];
                    int rIndex = (int)values[j];
                    if (rIndex > -1 && rIndex < semiMinorAxisBufferLength) {
                        semiMinorAxisBuffer[rIndex] += 1;
                    }
                }
            }
        }
        float bMax = -INFINITY;
        int iBMax = 0;
        for (int i = 0; i < semiMinorAxisBufferLength; i++) {
            if (semiMinorAxisBuffer[i] > bMax) {
                bMax = semiMinorAxisBuffer[i];
                iBMax = i;
            }
        }
        rep.semiMinorAxis = (float)iBMax;
        rep.semiMajorAxis = sqrtf(evl[1])/sqrtf(evl[0])*rep.semiMinorAxis;
        rep.angle = atan2f(evc[2], evc[0]);
    }
    
    return rep;
}

void EFT::cluster_by_ellipse(Point2D * centers, int nCenters, float proximityThreshold)
{
    ellipseIndices = (int *)malloc(nQuads*sizeof(int)); // to which ellipse index the pairs of triangles belong
    for (int i = 0; i < nQuads; i++) { ellipseIndices[i] = -1; }
    for (int i = 0; i < nQuads; i++) {
        Triangle t1 = triangles[tPairsInd1[i]];
        Triangle t2 = triangles[tPairsInd2[i]];
        LineIntersection li = lineintersection(t1.m, t1.d, t2.m, t2.d);
        float minDist = INFINITY;
        int iMinDist;
        for (int j = 0; j < nCenters; j++) {
            float dist;
            if (li.denominator != 0.0) {
                dist = norm(vsum(li.point, vnegative(centers[j])));
            } else {
                dist = INFINITY;
            }
            if (dist < minDist) {
                minDist = dist;
                iMinDist = j;
            }
        }
        if (minDist < proximityThreshold) {
            ellipseIndices[i] = iMinDist;
        }
    }
}

float arraymax(float * array, int size)
{
    float mx = -INFINITY;
    for (int i = 0; i < size; i++) {
        if (array[i] > mx) mx = array[i];
    }
    return mx;
}

float arraymin(float * array, int size)
{
    float mn = INFINITY;
    for (int i = 0; i < size; i++) {
        if (array[i] < mn) mn = array[i];
    }
    return mn;
}

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
//  Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
//  IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.
