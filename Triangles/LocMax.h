//
//  LocMax.h
//  Triangles
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#ifndef __Triangles__LocMax__
#define __Triangles__LocMax__

#include <iostream>
#include <vector>
#include <math.h>
#include <Accelerate/Accelerate.h>
#include "PATImage.h"
#include "LinAlg.h"

struct Kernel {
	int size;
    float * data;
};
typedef struct Kernel Kernel;

struct ComplexKernel {
	int size;
    float * dataR;
    float * dataI;
};
typedef struct ComplexKernel ComplexKernel;

enum WaveKernelType {
    WaveKernelTypeInteraction,
    WaveKernelTypeNoInteraction,
    WaveKernelTypeFull
};
typedef enum WaveKernelType WaveKernelType;

class LocMax {
    Kernel set_up_gauss_kernel(int halfSize, float stDev);
    Kernel set_up_wave_kernel(int halfSize, float stDev, float freq);
    Kernel set_up_2waves_kernel(int halfSize, float stDev1, float freq1, float stDev2, float freq2, WaveKernelType type);
    ComplexKernel set_up_2waves_complex_kernel(int halfSize, float stDev1, float freq1, float stDev2, float freq2);
    void clean_up_kernel(Kernel kernel);
    void clean_up_complex_kernel(ComplexKernel kernel);
    void convolve(PATImage input, Kernel kernel, PATImage output);
    void complex_mag_sqr(PATImage inputR, PATImage inputI, PATImage output);
    int nlm;
    float thr;
public:
    PATImage im;
    Point2D * locs;
    int nLocs; // number of local maxima actually found
    void set_up(PATImage image, int kernelHalfSize, int nLocMax, float threshold);
    void set_up_q(PATImage image, int kernelHalfSize, float stDev, float freq, int nLocMax, float threshold);
    void set_up_q2(PATImage image,
                   int kernelHalfSize,
                   float stDev1,
                   float freq1,
                   float stDev2,
                   float freq2,
                   WaveKernelType type,
                   int nLocMax,
                   float threshold);
    void set_up_q2_complex(PATImage image,
                           int kernelHalfSize,
                           float stDev1,
                           float freq1,
                           float stDev2,
                           float freq2,
                           int nLocMax,
                           float threshold);
    void find(void);
    void clean_up(void);
};

#endif /* defined(__Triangles__LocMax__) */

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
//  Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
//  IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.
