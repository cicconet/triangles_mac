//
//  Triangle.cpp
//  Triangles
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#include "Triangle.h"

void Triangle::set_up(float pX, float pY, float pA, float pM, float qX, float qY, float qA, float qM)
{
    p = Point2DMake(pX, pY);
    q = Point2DMake(qX, qY);
    tauP = Point2DMake(cosf(pA), sinf(pA));
    tauQ = Point2DMake(cosf(qA), sinf(qA));
    m = svproduct(0.5, vsum(p, q));
    
    LineIntersection li = lineintersection(p, tauP, q, tauQ);
    if (li.denominator != 0) {
        x = li.point;
    } else {
        x = Point2DMake(INFINITY, INFINITY);
    }
    
    base = norm(vsum(q, vnegative(p)));
    
    if (li.denominator != 0) {
        d = normalize(vsum(m, vnegative(x)));
    }
    
    Point2D q_minus_p = vsum(q, vnegative(p));
    T_pq = svproduct(1.0/norm(q_minus_p), q_minus_p);
    
    T_perp_pq = perp(T_pq);
    float theta = atan2f(T_perp_pq.y, T_perp_pq.x);
    Matrix2D S = matrix2d(cosf(2.0*theta), sinf(2.0*theta), sinf(2*theta), -cosf(2*theta));
    
    wmp = fabsf(dotproduct(tauQ, mvproduct(S, tauP))); // the closer to 1, the more mirror symmetric
    mp = 0.5*(1.0+dotproduct(tauQ, mvproduct(S, tauP))); // the closer to 1, the more mirror symmetric
    wpp = fabsf(dotproduct(tauQ, tauP)); // the closer to 1, the more parallel
    pp = 0.5*(1.0+dotproduct(tauQ, tauP)); // the closer to 1, the more parallel
    
//    wav_weight = (pM+qM)/2.0;
    wav_weight = pM*qM;
}

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
//  Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
//  IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.
