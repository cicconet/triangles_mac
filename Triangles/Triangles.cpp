//
//  Triangles.cpp
//  Triangles
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#include "Triangles.h"

void Triangles::set_up(int width,
                       int height,
                       float scale,
                       int nOrient,
                       int hopSize,
                       int halfWS,
                       float magThreshold,
                       bool ignoreDirection)
{
    histWidth = width;
    histHeight = height;
    PATCoefficients::set_up(width, height, scale, nOrient, hopSize, halfWS, magThreshold, true, true, ignoreDirection);
}

void Triangles::set_up_quick(int nCoef)
{
    nCoefficients = nCoef;
    nPairs = nCoefficients*(nCoefficients-1)/2;
    triangles = (Triangle *)malloc(nPairs*sizeof(Triangle));
}

void Triangles::list_triangles_quick(float *m, float *a, float *x, float *y)
{
    int pairIndex = -1;
    for (int i = 0; i < nCoefficients-1; i++) {
        for (int j = i+1; j < nCoefficients; j++) {
            pairIndex += 1;
            Triangle t;
            t.set_up(x[i], y[i], a[i], m[i], x[j], y[j], a[j], m[j]);
            triangles[pairIndex] = t;
        }
    }
}

void Triangles::clean_up_quick(void)
{
    free(triangles);
}

void Triangles::clean_up(void)
{
    if (triangles) { free(triangles); }
    PATCoefficients::clean_up();
}

void Triangles::list_triangles(void)
{
    // should only be called after PATCoefficients::find_coefficients
    nPairs = nCoefficients*(nCoefficients-1)/2;
    triangles = (Triangle *)malloc(nPairs*sizeof(Triangle));
    int pairIndex = -1;
    for (int i = 0; i < nCoefficients-1; i++) {
        for (int j = i+1; j < nCoefficients; j++) {
            pairIndex += 1;
            Triangle t;
            t.set_up(x[indices[i]], y[indices[i]], a[indices[i]], m[indices[i]], x[indices[j]], y[indices[j]], a[indices[j]], m[indices[j]]);
            triangles[pairIndex] = t;
        }
    }
}

void Triangles::hough_circ_tang(PATImage accSpace, float radMin, float radMax)
{
    for (int i = 0; i < nCoefficients; i++) {
        for (float rad = radMin; rad <= radMax; rad += 1.0) {
            int row = roundf(x[indices[i]]+rad*cosf(a[indices[i]]+M_PI_2));
            int col = roundf(y[indices[i]]+rad*sinf(a[indices[i]]+M_PI_2));
            if (row >= 0 && row < accSpace.height && col >= 0 && col < accSpace.width) {
                accSpace.data[row*accSpace.width+col] += m[indices[i]];
            }
            row = roundf(x[indices[i]]-rad*cosf(a[indices[i]]+M_PI_2));
            col = roundf(y[indices[i]]-rad*sinf(a[indices[i]]+M_PI_2));
            if (row >= 0 && row < accSpace.height && col >= 0 && col < accSpace.width) {
                accSpace.data[row*accSpace.width+col] += m[indices[i]];
            }
        }
    }
}

void Triangles::acc2(PATImage paramDomain,
                     Point2D(* location)(Triangle, int, int),
                     float(* weight)(Triangle, int, int))
{
    int row, col, index;
    for (int i = 0; i < nPairs; i++) {
        Triangle t = triangles[i];
        Point2D loc = (* location)(t, paramDomain.width, paramDomain.height);
        row = roundf(loc.x);
        col = roundf(loc.y);
        if (row > -1 && row < paramDomain.height && col > -1 && col < paramDomain.width) {
            float w = (* weight)(t, paramDomain.width, paramDomain.height);
            index = row*paramDomain.width+col;
            paramDomain.data[index] += w;
        }
    }
}

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
//  Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
//  IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.
