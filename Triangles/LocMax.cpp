//
//  LocMax.cpp
//  Triangles
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#include "LocMax.h"
#include "PATImage.h"

void LocMax::set_up(PATImage image, int kernelHalfSize, int nLocMax, float threshold)
{
    nlm = nLocMax;
    thr = threshold;
    
    locs = (Point2D *)malloc(nlm*sizeof(Point2D));
    
    im.set_up_with_data(NULL, image.width, image.height);
    Kernel kernel = set_up_gauss_kernel(kernelHalfSize, (float)kernelHalfSize/2.0);
    
    convolve(image, kernel, im);
    im.normalize();
    
    clean_up_kernel(kernel);
}

void LocMax::set_up_q(PATImage image, int kernelHalfSize, float stDev, float freq, int nLocMax, float threshold)
{
    nlm = nLocMax;
    thr = threshold;
    
    locs = (Point2D *)malloc(nlm*sizeof(Point2D));
    
    im.set_up_with_data(NULL, image.width, image.height);
    Kernel kernel = set_up_wave_kernel(kernelHalfSize, stDev, freq);
    
    convolve(image, kernel, im);
    im.normalize();
    
    clean_up_kernel(kernel);
}

void LocMax::set_up_q2(PATImage image,
                       int kernelHalfSize,
                       float stDev1,
                       float freq1,
                       float stDev2,
                       float freq2,
                       WaveKernelType type,
                       int nLocMax,
                       float threshold)
{
    nlm = nLocMax;
    thr = threshold;
    
    locs = (Point2D *)malloc(nlm*sizeof(Point2D));
    
    im.set_up_with_data(NULL, image.width, image.height);
    Kernel kernel = set_up_2waves_kernel(kernelHalfSize, stDev1, freq1, stDev2, freq2, type);
    
    convolve(image, kernel, im);
    im.normalize();
    
    clean_up_kernel(kernel);
}

void LocMax::set_up_q2_complex(PATImage image,
                       int kernelHalfSize,
                       float stDev1,
                       float freq1,
                       float stDev2,
                       float freq2,
                       int nLocMax,
                       float threshold)
{
    nlm = nLocMax;
    thr = threshold;
    
    locs = (Point2D *)malloc(nlm*sizeof(Point2D));
    
    im.set_up_with_data(NULL, image.width, image.height);
    
    PATImage outR; outR.set_up_with_data(NULL, image.width, image.height);
    PATImage outI; outI.set_up_with_data(NULL, image.width, image.height);
    
    ComplexKernel kernel = set_up_2waves_complex_kernel(kernelHalfSize, stDev1, freq1, stDev2, freq2);
    
    Kernel kR;
    kR.size = kernel.size;
    kR.data = kernel.dataR;
    
    Kernel kI;
    kI.size = kernel.size;
    kI.data = kernel.dataI;
    
    convolve(image, kR, outR);
    convolve(image, kI, outI);
    
    complex_mag_sqr(outR, outI, im);
    
    im.normalize();
    
    clean_up_complex_kernel(kernel);
}

void LocMax::complex_mag_sqr(PATImage inputR, PATImage inputI, PATImage output)
{
    for (int i = 0; i < output.height; i++) {
        for (int j = 0; j < output.width; j++) {
            int index = i*output.width+j;
            output.data[index] = inputR.data[index]*inputR.data[index]+inputI.data[index]*inputI.data[index];
        }
    }
}

void LocMax::clean_up(void)
{
    free(locs);
    im.clean_up();
}

void LocMax::find(void)
{
    // threshold should be in [0,1]
    std::vector<float> values;
    std::vector<Point2D> locations;
    for (int i = 1; i < im.height-1; i++) {
        for (int j = 1; j < im.width; j++) {
            float v = im.data[i*im.width+j];
            if (v > thr) {
                float vE = im.data[i*im.width+j+1];
                float vW = im.data[i*im.width+j-1];
                float vN = im.data[(i-1)*im.width+j];
                float vS = im.data[(i+1)*im.width+j];
                if (v > vE && v > vW && v > vN && v > vS) {
                    values.insert(values.end(), v);
                    locations.insert(locations.end(), Point2DMake(i, j));
                }
            }
        }
    }
    nLocs = nlm;
    if (values.size() < nLocs) {
        nLocs = (int)values.size();
    }
    for (int i = 0; i < nLocs; i++) {
        float max = -INFINITY;
        float jMax = 0;
        for (int j = 0; j < values.size(); j++) {
            float value = values.at(j);
            if (value > max) {
                max = value;
                jMax = j;
            }
        }
        locs[i] = Point2DMake(locations.at(jMax).x, locations.at(jMax).y);
        values.at(jMax) = 0.0;
    }
}

void LocMax::convolve(PATImage input, Kernel kernel, PATImage output)
{
    vImage_Buffer vImageBufferInput = input.v_image_buffer_structure();
    vImage_Buffer vImageBufferOutput = output.v_image_buffer_structure();
    
    vImageConvolve_PlanarF(&vImageBufferInput, &vImageBufferOutput, NULL, 0, 0, kernel.data, kernel.size, kernel.size, 0.0, kvImageBackgroundColorFill);
}

Kernel LocMax::set_up_gauss_kernel(int halfSize, float stDev)
{
    int size = 2*halfSize+1; // size should be odd
    
    Kernel kernel;
    kernel.size = size;
    kernel.data = (float *)malloc(size*size*sizeof(float));
    
    int i0 = size/2;
    int j0 = i0;
    float variance = stDev*stDev;
    float sum = 0.0;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            float xdist = i-i0;
            float ydist = j-j0;
            float value = expf(-0.5*(xdist*xdist+ydist*ydist)/variance);
            kernel.data[i*size+j] = value;
            sum += value;
        }
    }
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            kernel.data[i*size+j] /= sum;
        }
    }
    
//    PATImage image;
//    image.set_up_with_data(kernel.data, size, size);
//    image.normalize();
//    image.save_png_to_path("/Users/Cicconet/Desktop/Kernel.png");
//    image.clean_up();
    
    return kernel;
}

Kernel LocMax::set_up_wave_kernel(int halfSize, float stDev, float freq)
{
    int size = 2*halfSize+1; // size should be odd
    
    Kernel kernel;
    kernel.size = size;
    kernel.data = (float *)malloc(size*size*sizeof(float));
    
    Kernel gKernel = set_up_gauss_kernel(halfSize, stDev);
    int i0 = size/2;
    int j0 = i0;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            float xdist = i-i0;
            float ydist = j-j0;
            float dist = sqrtf(xdist*xdist+ydist*ydist);
            float value = cosf(freq*dist)*gKernel.data[i*size+j];
            kernel.data[i*size+j] = value;
        }
    }
    clean_up_kernel(gKernel);
    
//    PATImage image;
//    image.set_up_with_data(kernel.data, size, size);
//    image.normalize();
//    image.save_png_to_path("/Users/Cicconet/Desktop/Kernel.png");
//    image.clean_up();
    
    return kernel;
}

Kernel LocMax::set_up_2waves_kernel(int halfSize, float stDev1, float freq1, float stDev2, float freq2, WaveKernelType type)
{
    int size = 2*halfSize+1; // size should be odd
    
    Kernel kernel;
    kernel.size = size;
    kernel.data = (float *)malloc(size*size*sizeof(float));
    
    float df = freq1-freq2;
    
    int i0 = size/2;
    int j0 = i0;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            float xdist = i-i0;
            float ydist = j-j0;
            float dist = sqrtf(xdist*xdist+ydist*ydist);
            float value = 0.0;
            switch (type) {
                case WaveKernelTypeInteraction:
                    value = 2.0*cosf(df*dist)*expf(-0.25*dist*dist*(1.0/(stDev1*stDev1)+1.0/(stDev2*stDev2)));
                    break;
                case WaveKernelTypeNoInteraction:
//                    value = expf(-0.5*dist*dist/(stDev1*stDev1))+expf(-0.5*dist*dist/(stDev2*stDev2));
                    value = expf(-0.5*dist*dist/(stDev1*stDev1))/(stDev1*sqrtf(2.0*M_PI));
                    value += expf(-0.5*dist*dist/(stDev2*stDev2))/(stDev2*sqrtf(2.0*M_PI));
                    break;
                case WaveKernelTypeFull:
                    value = 2.0*cosf(df*dist)*expf(-0.25*dist*dist*(1.0/(stDev1*stDev1)+1.0/(stDev2*stDev2)));
                    value = value+expf(-0.5*dist*dist/(stDev1*stDev1))+expf(-0.5*dist*dist/(stDev2*stDev2));
                    break;
                default:
                    break;
            }
            kernel.data[i*size+j] = value;
        }
    }
    
//    PATImage image;
//    image.set_up_with_data(kernel.data, size, size);
//    image.normalize();
//    image.save_png_to_path("/Users/Cicconet/Desktop/Kernel.png");
//    image.clean_up();
    
    return kernel;
}

ComplexKernel LocMax::set_up_2waves_complex_kernel(int halfSize, float stDev1, float freq1, float stDev2, float freq2)
{
    int size = 2*halfSize+1; // size should be odd
    
    ComplexKernel kernel;
    kernel.size = size;
    kernel.dataR = (float *)malloc(size*size*sizeof(float));
    kernel.dataI = (float *)malloc(size*size*sizeof(float));
    
    int i0 = size/2;
    int j0 = i0;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            float xdist = i-i0;
            float ydist = j-j0;
            float dist = sqrtf(xdist*xdist+ydist*ydist);
            float sqrtG1 = sqrtf(expf(-0.5*dist*dist/(stDev1*stDev1))/(stDev1*sqrtf(2.0*M_PI)));
            float sqrtG2 = sqrtf(expf(-0.5*dist*dist/(stDev2*stDev2))/(stDev2*sqrtf(2.0*M_PI)));
            float w1R = cosf(freq1*dist);
            float w1I = sinf(freq1*dist);
            float w2R = cosf(freq2*dist);
            float w2I = sinf(freq2*dist);
            
            kernel.dataR[i*size+j] = sqrtG1*w1R+sqrtG2*w2R;
            kernel.dataI[i*size+j] = sqrtG1*w1I+sqrtG2*w2I;
        }
    }
    
//    PATImage image;
//    image.set_up_with_data(kernel.dataR, size, size);
//    image.normalize();
//    image.save_png_to_path("/Users/Cicconet/Desktop/KernelR.png");
//    image.clean_up();
//    image.set_up_with_data(kernel.dataI, size, size);
//    image.normalize();
//    image.save_png_to_path("/Users/Cicconet/Desktop/KernelI.png");
//    image.clean_up();
    
    return kernel;
}

void LocMax::clean_up_kernel(Kernel kernel)
{
    free(kernel.data);
}

void LocMax::clean_up_complex_kernel(ComplexKernel kernel)
{
    free(kernel.dataR);
    free(kernel.dataI);
}

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
//  Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
//  IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.
