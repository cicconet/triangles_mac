//
//  EFT.h
//  Triangles
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#ifndef __Triangles__EFT__
#define __Triangles__EFT__

#include <iostream>
#include "Triangles.h"

typedef struct RemEllPar {
    float semiMinorAxis;
    float semiMajorAxis;
    float angle;
} RemEllPar;

class EFT : public Triangles {
    int nQuads;
    int * tPairsInd1;
    int * tPairsInd2;
    int * ellipseIndices;
    float * semiMinorAxisBuffer;
    int semiMinorAxisBufferLength;
public:
    void set_up(int width,
                int height,
                float scale,
                int nOrient,
                int hopSize,
                int halfWS,
                float magThreshold,
                bool ignoreDirection);
    void compute_centers_histogram(PATImage centHist, float minRadius, float maxRadius);
    void cluster_by_ellipse(Point2D * centers, int nCenters, float proximityThreshold);
    void paint_points_in_ellipse(PATImage image, int ellipseIndex);
    RemEllPar remaining_parameters_in_ellipse(int ellipseIndex, Point2D center);
    void clean_up(void);
};

#endif /* defined(__Triangles__EFT__) */

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
//  Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
//  IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.
